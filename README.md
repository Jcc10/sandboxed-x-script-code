# Sandboxed X-Script Code

A way to sandbox [Java/ECMA/Type]Script code (supplied by users). Originally designed for creating interactive tutorials on JavaScript programming.